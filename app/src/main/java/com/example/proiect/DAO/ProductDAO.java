package com.example.proiect.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.proiect.Entities.Product;
import com.example.proiect.Entities.Subcategory;

import java.util.List;

@Dao
public interface ProductDAO {

    //GET Methods
    @Query("SELECT * FROM Products")
    public List<Product> getAllProducts();
    @Query("SELECT * FROM Products WHERE subcategoryParentId=:subcategory_id")
    public List<Product> getProductsBySubcategoryId(int subcategory_id);

    @Query("SELECT * FROM Products WHERE productId=:id")
    public Product getProductById(int id);

    @Query("SELECT * FROM Products WHERE CHARINDEX(:name, Name) > 0")
    public List<Product> getProductByName(String name);

    //POST Methods
    @Insert
    void insertProducts(Product... products);

    //DELETE Methods
    @Delete
    void delete(Product product);
}
