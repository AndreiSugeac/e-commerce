package com.example.proiect.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Transaction;

import com.example.proiect.Entities.Category;
import com.example.proiect.Entities.Relations.CategoryWithSubcategories;
import com.example.proiect.Entities.Subcategory;

import java.util.List;

@Dao
public interface SubcategoryDAO {
    //GET Methods
    @Query("SELECT * FROM Subcategories")
    public List<Subcategory> getSubcategories();

    @Query("SELECT * FROM Subcategories WHERE Category=:category_id")
    public List<Subcategory> getSubcategoriesByCategoryId(int category_id);

    @Query("SELECT * FROM Subcategories WHERE Name=:name")
    public Subcategory getSubcategoryByName(String name);

    //POST Methods
    @Insert
    public void insertSubcategories(Subcategory...subCategories);

    //DELETE Methods
    @Delete
    public void delete(Subcategory subCategory);
}
