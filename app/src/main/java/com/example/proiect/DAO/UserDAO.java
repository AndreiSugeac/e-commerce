package com.example.proiect.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.proiect.Entities.User;

import java.util.List;

@Dao
public interface UserDAO {

    // GET METHODS
    @Query("SELECT * FROM Users")
    List<User> getAllUsers();

    @Query("SELECT * FROM Users WHERE userId = :id")
    public User getUserById(int id);

    @Query("SELECT * FROM Users WHERE Email=:email AND Password=:password")
    User login(String email, String password);

    // POST METHODS
    @Insert
    void insertMultiple(User... users);

    //DELETE METHODS
    @Delete
    public void delete(User user);


}
