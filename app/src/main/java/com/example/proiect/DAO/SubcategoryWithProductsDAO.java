package com.example.proiect.DAO;

import androidx.room.Query;
import androidx.room.Transaction;

import com.example.proiect.Entities.Relations.SubcategoryWithProducts;

import java.util.List;

public interface SubcategoryWithProductsDAO {
    @Transaction
    @Query("SELECT * FROM Subcategories")
    public List<SubcategoryWithProducts> getSubcategoriesWithProducts();
}
