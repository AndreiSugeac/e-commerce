package com.example.proiect.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.proiect.Entities.Category;

import java.util.List;

@Dao
public interface CategoryDAO {
    //GET Methods
    @Query("SELECT * FROM Categories")
    public List<Category> getCategories();

    @Query("SELECT * FROM Categories WHERE Name=:name")
    public Category getCategoryByName(String name);

    //POST Methods
    @Insert
    public void insertCategories(Category...categories);

    //DELETE Methods
    @Delete
    public void delete(Category category);
}
