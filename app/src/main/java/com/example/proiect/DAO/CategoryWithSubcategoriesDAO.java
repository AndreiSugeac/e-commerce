package com.example.proiect.DAO;

import androidx.room.Query;
import androidx.room.Transaction;

import com.example.proiect.Entities.Relations.CategoryWithSubcategories;

import java.util.List;

public interface CategoryWithSubcategoriesDAO {
    @Transaction
    @Query("SELECT * FROM Categories")
    public List<CategoryWithSubcategories> getCategoriesWithSubcategories();
}
