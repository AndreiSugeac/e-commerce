package com.example.proiect;

import android.app.Application;

import androidx.room.Room;

import com.example.proiect.DB.Database;

public class App extends Application {

    private static App appInstance;
    private static Database appDatabase;

    @Override
    public void onCreate() {
        super.onCreate();

        appInstance = this;
        appDatabase = Room.databaseBuilder(
                getApplicationContext(),
                Database.class,
                "e-commerce-db"
        ).build();
    }

    public static App getInstance() {
        return appInstance;
    }

    public static Database getAppDatabase() {
        return appDatabase;
    }
}
