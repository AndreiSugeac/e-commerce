package com.example.proiect.Entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName ="Products")
public class Product {

    @PrimaryKey(autoGenerate = true)
    public int productId;

    @ColumnInfo(name = "Name")
    public String productName;

    @ColumnInfo(name = "Description")
    public String productDescription;

    @ColumnInfo(name = "Price")
    public int productPrice;

    @ColumnInfo(name = "In stock")
    public int inStock;

    public int subcategoryParentId;

    public Product(String productName, String productDescription, int productPrice, int inStock, int subcategoryParentId) {
        this.productName = productName;
        this.productDescription = productDescription;
        this.productPrice = productPrice;
        this.inStock = inStock;
        this.subcategoryParentId = subcategoryParentId;
    }
}
