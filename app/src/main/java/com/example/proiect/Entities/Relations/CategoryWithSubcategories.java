package com.example.proiect.Entities.Relations;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.example.proiect.Entities.Category;
import com.example.proiect.Entities.Subcategory;

import java.util.List;

public class CategoryWithSubcategories {
    @Embedded
    public Category category;

    @Relation(
            parentColumn = "categoryId",
            entityColumn = "categoryParentId"
    )
    public List<Subcategory> subcategories;
}
