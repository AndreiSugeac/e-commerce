package com.example.proiect.Entities.Relations;

import androidx.room.Embedded;
import androidx.room.Relation;

import com.example.proiect.Entities.Product;
import com.example.proiect.Entities.Subcategory;

import java.util.List;

public class SubcategoryWithProducts {
    @Embedded
    public Subcategory subcategory;

    @Relation(
            parentColumn = "subcategoryId",
            entityColumn = "subcategoryParentId"
    )
    public List<Product> products;
}
