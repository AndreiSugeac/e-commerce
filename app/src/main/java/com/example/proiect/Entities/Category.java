package com.example.proiect.Entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Categories")
public class Category {
    @PrimaryKey(autoGenerate = true)
    public int categoryId;

    @ColumnInfo(name = "Name")
    public String categoryName;

    public Category(String categoryName) {
        this.categoryName = categoryName;
    }
}
