package com.example.proiect.Entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Subcategories")
public class Subcategory {
    @PrimaryKey(autoGenerate = true)
    public int subcategoryId;

    @ColumnInfo(name = "Category")
    public int categoryParentId;

    @ColumnInfo(name = "Name")
    public String subcategoryName;

    public Subcategory(int categoryParentId, String subcategoryName) {
        this.categoryParentId = categoryParentId;
        this.subcategoryName = subcategoryName;
    }
}
