package com.example.proiect.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.service.autofill.UserData;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.proiect.DAO.UserDAO;
import com.example.proiect.DB.Database;
import com.example.proiect.Entities.User;
import com.example.proiect.R;
import com.example.proiect.Activities.Home;
import com.example.proiect.Screens.HomeFragment;
import com.example.proiect.Screens.Register;
import com.example.proiect.Services.UserServices.InsertUserService;
import com.example.proiect.Services.UserServices.LoginUserService;
import com.example.proiect.Services.UserServices.UserServices;

public class MainActivity extends AppCompatActivity implements UserServices{

    public static final String PREFERENCES_KEY = "com.example.proiect.preferences";

    Button login, register;

    EditText email, password;

    String getEmailInputText() {
        return email.getText().toString();
    }

    String getPasswordInputText() {
        return password.getText().toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences preferences = getSharedPreferences(PREFERENCES_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        boolean isLoggedIn = preferences.getInt("UserId", -1) != -1;

        if(isLoggedIn) {
            Intent myIntent = new Intent(getApplicationContext(), Home.class);
            startActivityForResult(myIntent, 0);
            finish();
        }
        setContentView(R.layout.activity_main);

        email = (EditText)findViewById(R.id.email);
        password = (EditText)findViewById(R.id.password);

        login = (Button) findViewById(R.id.login);
        login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                login(view);
            }
        });

        register = (Button) findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), Register.class);
                startActivityForResult(myIntent, 0);
                finish();
            }
        });
    }

    public void login(View view) {
       new LoginUserService(this, getApplicationContext()).execute(new String[]{getEmailInputText(), getPasswordInputText()});
        Intent myIntent = new Intent(view.getContext(), Home.class);
        startActivityForResult(myIntent, 0);
    }

    @Override
    public void userOperationResult(String result) {
        Toast.makeText(this, result, Toast.LENGTH_LONG).show();
    }


}