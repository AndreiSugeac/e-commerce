package com.example.proiect.Activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.proiect.R;

import java.util.Timer;
import java.util.TimerTask;

public class FirstScreenAct extends AppCompatActivity {
    Timer timer;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_screen);

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Intent intent = new Intent(FirstScreenAct.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, 2000);
    }

}
