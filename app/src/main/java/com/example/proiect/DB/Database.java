package com.example.proiect.DB;

import androidx.room.RoomDatabase;

import com.example.proiect.DAO.CategoryDAO;
import com.example.proiect.DAO.ProductDAO;
import com.example.proiect.DAO.SubcategoryDAO;
import com.example.proiect.DAO.UserDAO;
import com.example.proiect.Entities.Category;
import com.example.proiect.Entities.Product;
import com.example.proiect.Entities.Subcategory;
import com.example.proiect.Entities.User;

@androidx.room.Database(entities = {User.class, Product.class, Subcategory.class, Category.class}, version = 1)
public abstract class Database extends RoomDatabase {

    public abstract UserDAO userDAO();

    public abstract ProductDAO productDAO();

    public abstract CategoryDAO categoryDAO();

    public abstract SubcategoryDAO subcategoryDAO();
}
