package com.example.proiect.Services.ProductServices;

import android.os.AsyncTask;
import android.widget.Toast;

import com.example.proiect.App;
import com.example.proiect.Entities.Product;

public class InsertProductService extends AsyncTask<Product, Object, String> {

    ProductServices listener;

    public InsertProductService(ProductServices listener) {
        this.listener = listener;
    }

    @Override
    protected String doInBackground(Product... products) {
        try {
            App.getAppDatabase().productDAO().insertProducts(products);
        } catch (Exception e) {
            e.printStackTrace();
            return "Failed to insert products!";
        }

        return "Successfully inserted products!";
    }

    @Override
    protected void onProgressUpdate(Object[] values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String result) {
        this.listener.productOperationResult(result);
    }
}
