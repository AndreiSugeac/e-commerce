package com.example.proiect.Services.CategoryServices;

import android.os.AsyncTask;

import com.example.proiect.App;
import com.example.proiect.Entities.Category;
import com.example.proiect.Screens.HomeFragment;

import java.util.ArrayList;
import java.util.List;

public class GetCategoriesService extends AsyncTask<Category, Object, List<Category>>{

    @Override
    protected List<Category> doInBackground(Category...categories) {
        try{
            List<Category> temp = new ArrayList<Category>();
            temp = App.getAppDatabase().categoryDAO().getCategories();
            return temp;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
    @Override
    protected void onProgressUpdate(Object[] values) {
        super.onProgressUpdate(values);
    }
    @Override
    protected void onPostExecute(List<Category> result) {
        HomeFragment.category_list = result;
    }
}
