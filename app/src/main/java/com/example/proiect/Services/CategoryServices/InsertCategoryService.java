package com.example.proiect.Services.CategoryServices;

import android.os.AsyncTask;

import com.example.proiect.App;
import com.example.proiect.Entities.Category;

public class InsertCategoryService extends AsyncTask<Category, Object, String> {

    CategoryServices listener;

    public InsertCategoryService(CategoryServices listener) {
        this.listener = listener;
    }

    @Override
    protected String doInBackground(Category...categories) {
            try {
            App.getAppDatabase().categoryDAO().insertCategories(categories);
            } catch (Exception e) {
            e.printStackTrace();
            return "Failed to insert products!";
            }

            return "Successfully inserted products!";
            }

    @Override
    protected void onProgressUpdate(Object[] values) {
            super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String result) {
            this.listener.categoryOperationResult(result);
    }
}
