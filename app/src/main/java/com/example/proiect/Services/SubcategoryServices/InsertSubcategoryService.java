package com.example.proiect.Services.SubcategoryServices;

import android.os.AsyncTask;

import com.example.proiect.App;
import com.example.proiect.Entities.Subcategory;

public class InsertSubcategoryService extends AsyncTask<Subcategory, Object, String> {

    SubcategoryServices listener;

    public InsertSubcategoryService(SubcategoryServices listener) {
        this.listener = listener;
    }

    @Override
    protected String doInBackground(Subcategory...subCategories) {
        try {
            App.getAppDatabase().subcategoryDAO().insertSubcategories(subCategories);
        } catch (Exception e) {
            e.printStackTrace();
            return "Failed to insert products!";
        }

        return "Successfully inserted products!";
    }

    @Override
    protected void onProgressUpdate(Object[] values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String result) {
        this.listener.subcategoryOperationResult(result);
    }
}
