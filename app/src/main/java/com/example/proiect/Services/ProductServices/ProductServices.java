package com.example.proiect.Services.ProductServices;

import com.example.proiect.Entities.Product;

import java.util.List;

public interface ProductServices {

    void productOperationResult(String result);

    void getProductsBySubcategoryId(List<Product> productList);
}
