package com.example.proiect.Services.ProductServices;

import android.os.AsyncTask;

import com.example.proiect.App;
import com.example.proiect.Entities.Product;
import com.example.proiect.Entities.Subcategory;
import com.example.proiect.Services.SubcategoryServices.SubcategoryServices;

import java.util.ArrayList;
import java.util.List;

public class GetProductsBySubcategoryIdService extends AsyncTask<Object, Object, List<Product>> {
    ProductServices listener;

    public GetProductsBySubcategoryIdService(ProductServices productServices) {
        this.listener = productServices;
    }

    @Override
    protected List<Product> doInBackground(Object[] objects) {
        try{
            List<Product> temp = new ArrayList<>();
            temp = App.getAppDatabase().productDAO().getProductsBySubcategoryId((int)objects[0]);
            return temp;
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(List<Product> productList) {
        listener.getProductsBySubcategoryId(productList);
    }
}
