package com.example.proiect.Services.UserServices;

import android.os.AsyncTask;
import android.widget.Toast;

import com.example.proiect.App;
import com.example.proiect.Entities.User;

public class InsertUserService extends AsyncTask<User, Object, String> {

    UserServices listener;

    public InsertUserService(UserServices listener) {
        this.listener = listener;
    }

    @Override
    protected String doInBackground(User... users) {
        try {
            App.getAppDatabase().userDAO().insertMultiple(users);
        } catch(Exception e) {
            return "error";
        }
        return "success";
    }

    @Override
    protected void onProgressUpdate(Object[] values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String result) {
        listener.userOperationResult(result);
    }
}
