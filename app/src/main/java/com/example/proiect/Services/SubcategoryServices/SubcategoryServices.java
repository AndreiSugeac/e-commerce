package com.example.proiect.Services.SubcategoryServices;

import com.example.proiect.Entities.Subcategory;

import java.util.List;

public interface SubcategoryServices {
    public void subcategoryOperationResult(String result);

    public void getSubcategoriesByParentId(List<Subcategory> subcategoryList);
}
