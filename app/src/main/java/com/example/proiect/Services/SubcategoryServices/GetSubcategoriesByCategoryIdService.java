package com.example.proiect.Services.SubcategoryServices;

import android.os.AsyncTask;

import com.example.proiect.App;
import com.example.proiect.Entities.Subcategory;

import java.util.ArrayList;
import java.util.List;

public class GetSubcategoriesByCategoryIdService extends AsyncTask<Object, Object, List<Subcategory>> {

    SubcategoryServices listener;

    public GetSubcategoriesByCategoryIdService(SubcategoryServices subcategoryServices) {
        this.listener = subcategoryServices;
    }

    @Override
    protected List<Subcategory> doInBackground(Object[] objects) {
            try{
                List<Subcategory> temp = new ArrayList<>();
                temp = App.getAppDatabase().subcategoryDAO().getSubcategoriesByCategoryId((int)objects[0]);
                return temp;
            }catch(Exception e){
                e.printStackTrace();
                return null;
            }
    }

    @Override
    protected void onPostExecute(List<Subcategory> subcategoryList) {
        listener.getSubcategoriesByParentId(subcategoryList);
    }
}
