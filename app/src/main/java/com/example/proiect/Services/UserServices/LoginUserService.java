package com.example.proiect.Services.UserServices;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.example.proiect.App;
import com.example.proiect.Entities.User;

public class LoginUserService extends AsyncTask<String, Object, User> {

    UserServices listener;
    Context context;

    User loggedUser;

    public User getLoggedUser() {
        return loggedUser;
    }

    public LoginUserService(UserServices listener, Context context) {
        this.listener = listener;
        this.context = context;
        loggedUser = null;
    }

    @Override
    protected User doInBackground(String[] args) {
        User loggedUser;
        try {
            loggedUser = App.getAppDatabase().userDAO().login(args[0], args[1]);
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
        return loggedUser;
    }

    @Override
    protected void onProgressUpdate(Object[] values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(User loggedUser) {
        if(loggedUser != null) {
            SharedPreferences preferences = context.getSharedPreferences("com.example.proiect.preferences", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt("UserId", loggedUser.getId());
            editor.apply();
            editor.putString("UserFirstName", loggedUser.getFirstName());
            editor.apply();
            editor.putString("UserLastName", loggedUser.getLastName());
            editor.apply();
            editor.putString("UserEmail", loggedUser.getEmail());
            editor.apply();
            listener.userOperationResult("Successful login!");
        }
        else {
            listener.userOperationResult("Login error!");
        }
    }
}
