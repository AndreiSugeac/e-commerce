package com.example.proiect.Screens;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Entity;

import com.example.proiect.App;
import com.example.proiect.DAO.CategoryDAO;
import com.example.proiect.Entities.Category;
import com.example.proiect.R;
import com.example.proiect.Services.CategoryServices.GetCategoriesService;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment implements OnItemClickListener<Category>{

    public static List<Category> category_list = new ArrayList<>();

    public HomeFragment(){
        super(R.layout.fragment_home);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initializeCategoryList();
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        CategoryAdapter categoryAdapter = new CategoryAdapter(category_list, this);
        recyclerView.setAdapter(categoryAdapter);
    }
    private void initializeCategoryList(){

        new GetCategoriesService().execute();
    }

    @Override
    public void onItemClick(Category item) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager() ;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction() ;
        fragmentTransaction.replace(R.id. fragment_container , new SubcategoriesFragment(item.categoryId))
                .addToBackStack( null ) ;
        fragmentTransaction.commit() ;
    }
}
