package com.example.proiect.Screens;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Entity;

import com.example.proiect.Entities.Subcategory;
import com.example.proiect.R;

import java.util.List;

public class SubcategoriesAdapter extends RecyclerView.Adapter<SubcategoriesAdapter.SubcategoryViewHolder>{
    private List<Subcategory> dataSet;
    public static OnItemClickListener itemClickListener;
    // the adapter takes the list: creates a viewholder, assigns it an element and bind...

    public SubcategoriesAdapter(List<Subcategory> data,  OnItemClickListener listener){ // + , OnItemClickListener listener
        dataSet = data;
        itemClickListener = listener;
    }
    @NonNull
    @Override
    public SubcategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_subcategory, parent, false);
        return new SubcategoryViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull SubcategoriesAdapter.SubcategoryViewHolder holder, int position) {

        holder.Bind(dataSet.get(position));
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class SubcategoryViewHolder extends RecyclerView.ViewHolder{

        private final TextView textView;
        private final ImageView imageView;
        private final ConstraintLayout subcategoryContainer;


        public SubcategoryViewHolder(View view){
            super(view);

            textView = view.findViewById(R.id.title);
            imageView = view.findViewById(R.id.image);
            subcategoryContainer= view.findViewById(R.id.subcategory_container);
        }
        public void Bind(Subcategory item){

            textView.setText(item.subcategoryName);
            switch (item.subcategoryId){
                        case 1:
                            imageView.setImageResource(R.drawable.laptops);
                            break;
                        case 2:
                            imageView.setImageResource(R.drawable.electronics);
                            break;
                        case 3:
                            imageView.setImageResource(R.drawable.tablets);
                            break;
                        case 4:
                            imageView.setImageResource(R.drawable.playstations);
                            break;
                        case 5:
                            imageView.setImageResource(R.drawable.tv);
                            break;
                        case 6:
                            imageView.setImageResource(R.drawable.camera);
                            break;
                        case 7:
                            imageView.setImageResource(R.drawable.jackets);
                            break;
                        case 8:
                            imageView.setImageResource(R.drawable.sweaters);
                            break;
                        case 9:
                            imageView.setImageResource(R.drawable.tshirts);
                            break;
                        case 10:
                            imageView.setImageResource(R.drawable.pants);
                            break;
                        case 11:
                            imageView.setImageResource(R.drawable.shoes);
                            break;
                        case 12:
                            imageView.setImageResource(R.drawable.garden);
                            break;
                        case 13:
                            imageView.setImageResource(R.drawable.furniture);
                            break;
                        case 14:
                            imageView.setImageResource(R.drawable.bathroom);
                            break;
                        case 15:
                            imageView.setImageResource(R.drawable.kitchen);
                            break;
                        case 16:
                            imageView.setImageResource(R.drawable.clothes_icon);
                            break;
                        case 17:
                            imageView.setImageResource(R.drawable.face);
                            break;
                        case 18:
                            imageView.setImageResource(R.drawable.cosmetics);
                            break;
                        case 19:
                            imageView.setImageResource(R.drawable.hair);
                            break;
                        case 20:
                            imageView.setImageResource(R.drawable.oralhygiene);
                            break;
            }

            subcategoryContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // announce fragment that we've clicked on an element
                    //triggers onItemClick in fragment
                    itemClickListener.onItemClick(item);
                }
            });
        }
    }

}
