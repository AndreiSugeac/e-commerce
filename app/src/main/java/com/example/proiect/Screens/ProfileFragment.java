package com.example.proiect.Screens;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.proiect.Activities.MainActivity;
import com.example.proiect.R;
import com.example.proiect.Utilities.Preferences;

import org.w3c.dom.Text;

public class ProfileFragment extends Fragment {

    public Button logoutButton, ordersButton;
    public TextView userName;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        SharedPreferences preferences = getContext().getSharedPreferences(Preferences.key, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        userName = (TextView)view.findViewById(R.id.UserName);
        String firstName = preferences.getString("UserFirstName", null);
        String lastName = preferences.getString("UserLastName", null);
        userName.setText(firstName + " " + lastName);
        logoutButton = (Button)view.findViewById(R.id.logoutBtn);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.clear();
                editor.apply();
                Intent myIntent = new Intent(getContext(), MainActivity.class);
                startActivityForResult(myIntent, 0);
            }
        });

        return view;
    }
}
