package com.example.proiect.Screens;

import androidx.room.Entity;

import com.example.proiect.Entities.Category;
import com.example.proiect.Entities.Subcategory;

public interface OnItemClickListener<T> {

    void onItemClick(T item);
}
