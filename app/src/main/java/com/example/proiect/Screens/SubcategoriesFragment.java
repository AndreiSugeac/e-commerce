package com.example.proiect.Screens;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;


import com.example.proiect.Entities.Subcategory;
import com.example.proiect.R;
import com.example.proiect.Services.SubcategoryServices.GetSubcategoriesByCategoryIdService;
import com.example.proiect.Services.SubcategoryServices.SubcategoryServices;

import java.util.ArrayList;
import java.util.List;

public class SubcategoriesFragment extends Fragment implements SubcategoryServices, OnItemClickListener<Subcategory> {


    public List<Subcategory> subcategory_list = new ArrayList<>();
    public int category_id;
    public static AsyncTask<Object, Object, List<Subcategory>> GetSubcategoriesByCategoryId;
    public SubcategoriesAdapter subcategoriesAdapter;
    public RecyclerView recyclerView;

    public SubcategoriesFragment(int category_id){
        super(R.layout.fragment_subcategories);
        subcategoriesAdapter = new SubcategoriesAdapter(subcategory_list, this);
        GetSubcategoriesByCategoryId = new GetSubcategoriesByCategoryIdService(this);
        this.category_id = category_id;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initializeSubcategoryList();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try{
            GetSubcategoriesByCategoryId.get();
            recyclerView = view.findViewById(R.id.recycler_view);
            //+ this when add onitemclick method
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeSubcategoryList() {
        try{
            GetSubcategoriesByCategoryId.execute(category_id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void subcategoryOperationResult(String result) {
        Toast.makeText(getContext(), result, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getSubcategoriesByParentId(List<Subcategory> subcategoryList) {
        this.subcategory_list = subcategoryList;
        subcategoriesAdapter = new SubcategoriesAdapter(subcategory_list, this);
        subcategoriesAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(subcategoriesAdapter);
    }

    @Override
    public void onItemClick(Subcategory item) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager() ;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction() ;
        fragmentTransaction.replace(R.id. fragment_container , new ProductsFragment(item.subcategoryId))
                .addToBackStack( null ) ;
        fragmentTransaction.commit() ;
    }
}
