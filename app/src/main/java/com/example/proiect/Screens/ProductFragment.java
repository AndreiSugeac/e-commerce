package com.example.proiect.Screens;

import android.media.Image;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.proiect.Entities.Product;
import com.example.proiect.R;
import com.example.proiect.Services.CategoryServices.GetCategoriesService;
import com.example.proiect.Services.ProductServices.GetProductsBySubcategoryIdService;

public class ProductFragment extends Fragment {

    public Product product;
    public int product_id;
    public String name;
    public int price;
    public String description;
    public TextView title;
    public ImageView image;
    public TextView price_;
    public TextView description_;
    public Button addToCartBtn;
    private View view;

    public ProductFragment(Product product) {
        super(R.layout.fragment_product);
        this.product = product;
        this.product_id = product.productId;
        this.name = product.productName;
        this.price = product.productPrice;
        this.description = product.productDescription;
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_product, container, false);
        addToCartBtn = view.findViewById(R.id.addToCartBtn);
        addToCartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addProductToCart();
            }
        });
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        title = (TextView) view.findViewById(R.id.title);
        title.setText(String.valueOf(name));
        image = (ImageView) view.findViewById(R.id.image);
        price_ = (TextView) view.findViewById(R.id.price);
        price_.setText(String.valueOf(price));
        description_ = (TextView) view.findViewById(R.id.description);
        description_.setText(String.valueOf(description));
        switch (product_id){
            case 1:
                image.setImageResource(R.drawable.macbook);
                break;
            case 2:
                image.setImageResource(R.drawable.asus);
                break;
            case 3:
                image.setImageResource(R.drawable.lenovo);
                break;
            case 4:
                image.setImageResource(R.drawable.iphone);
                break;
            case 5:
                image.setImageResource(R.drawable.samsung);
                break;
            case 6:
                image.setImageResource(R.drawable.huawei);
                break;
        }
    }

    public void addProductToCart() {
        ShoppingCartFragment.productsInCart.add(product);
    }

}
