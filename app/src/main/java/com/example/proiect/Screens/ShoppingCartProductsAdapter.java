package com.example.proiect.Screens;

import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.proiect.Entities.Product;
import com.example.proiect.R;

import org.w3c.dom.Text;

import java.util.List;

public class ShoppingCartProductsAdapter extends RecyclerView.Adapter<ShoppingCartProductsAdapter.ShoppingCartViewHolder>{

    public List<Product> products;
    public static OnItemClickListener itemClickListener;
    public static View fragmentView;
    public ShoppingCartProductsAdapter(List<Product> data, OnItemClickListener listener, View view) {
        products = data;
        itemClickListener = listener;
        fragmentView = view;
    }

    @NonNull
    @Override
    public ShoppingCartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_shopping_cart, parent, false);
        return new ShoppingCartViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShoppingCartViewHolder holder, int position) {
        holder.Bind(products.get(position));
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public static class ShoppingCartViewHolder extends RecyclerView.ViewHolder {

        public TextView cartTotal = ShoppingCartProductsAdapter.fragmentView.findViewById(R.id.Total);
        public static int cartTotalSum = 0;
        public TextView prodName, qty, total;
        public int qtyNumber = 1;
        public int totalSum;
        public ImageView minusBtn, plusBtn, removeBtn;

        public ShoppingCartViewHolder(@NonNull View itemView) {
            super(itemView);
            this.prodName = itemView.findViewById(R.id.product_name);
            this.qty = itemView.findViewById(R.id.product_qty);
            this.total = itemView.findViewById(R.id.total);
            this.minusBtn = itemView.findViewById(R.id.minusBtn);
            this.plusBtn = itemView.findViewById(R.id.plusBtn);
            this.removeBtn = itemView.findViewById(R.id.removeBtn);
        }

        public void Bind(Product product) {
            prodName.setText(product.productName);
            qty.setText(Integer.toString(qtyNumber));
            totalSum = qtyNumber * product.productPrice;
            total.setText(Integer.toString(totalSum) + '$');
            cartTotalSum += totalSum;
            cartTotal.setText(Integer.toString(cartTotalSum) + '$');

            minusBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(qtyNumber > 1) {
                        qtyNumber -= 1;
                        qty.setText(Integer.toString(qtyNumber));
                        totalSum -=  product.productPrice;
                        total.setText(Integer.toString(totalSum) + "$");
                        cartTotalSum -= product.productPrice;
                        cartTotal.setText(Integer.toString(cartTotalSum) + '$');

                    }
                }
            });

            plusBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    qtyNumber += 1;
                    qty.setText(Integer.toString(qtyNumber));
                    totalSum +=  product.productPrice;
                    total.setText(Integer.toString(totalSum) + "$");
                    cartTotalSum += product.productPrice;
                    cartTotal.setText(Integer.toString(cartTotalSum) + '$');
                }
            });

            removeBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cartTotalSum -= totalSum;
                    cartTotal.setText(Integer.toString(cartTotalSum) + '$');
                    itemClickListener.onItemClick(product);
                }
            });

        }
    }
}
