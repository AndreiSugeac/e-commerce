package com.example.proiect.Screens;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.proiect.Entities.Product;
import com.example.proiect.Entities.Subcategory;
import com.example.proiect.R;
import com.example.proiect.Services.ProductServices.GetProductsBySubcategoryIdService;
import com.example.proiect.Services.ProductServices.ProductServices;
import com.example.proiect.Services.SubcategoryServices.GetSubcategoriesByCategoryIdService;

import java.util.ArrayList;
import java.util.List;

public class ProductsFragment extends Fragment implements ProductServices, OnItemClickListener<Product> {

    public List<Product> product_list = new ArrayList<>();
    public int subcategory_id;
    public static AsyncTask<Object, Object, List<Product>> GetProductsBySubcategoryId;
    public ProductsAdapter productsAdapter;
    public RecyclerView recyclerView;

    public ProductsFragment(int subcategory_id){
        super(R.layout.fragment_products);
        productsAdapter = new ProductsAdapter(product_list, this);
        GetProductsBySubcategoryId = new GetProductsBySubcategoryIdService(this);
        this.subcategory_id = subcategory_id;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initializeProductList();
        View view = super.onCreateView(inflater, container, savedInstanceState);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try{
            GetProductsBySubcategoryId.get();
            recyclerView = view.findViewById(R.id.recycler_view);
            //+ this when add onitemclick method
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeProductList() {
        try{
            GetProductsBySubcategoryId.execute(subcategory_id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void productOperationResult(String result) {
        Toast.makeText(getContext(), result, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getProductsBySubcategoryId(List<Product> productList) {

        this.product_list = productList;
        productsAdapter = new ProductsAdapter(product_list, this);
        productsAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(productsAdapter);
    }

      @Override
    public void onItemClick(Product item) {

          FragmentManager fragmentManager = getActivity().getSupportFragmentManager() ;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction() ;
        fragmentTransaction.replace(R.id. fragment_container , new ProductFragment(item))
                .addToBackStack( null ) ;
        fragmentTransaction.commit() ;
    }

/*    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Home.this, NextActivity.class);
        startActivity(intent);
        finish();
    }*/
}
