package com.example.proiect.Screens;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.proiect.App;
import com.example.proiect.DAO.CategoryDAO;
import com.example.proiect.Entities.Category;
import com.example.proiect.Entities.Product;
import com.example.proiect.Entities.Subcategory;
import com.example.proiect.R;
import com.example.proiect.Services.CategoryServices.CategoryServices;
import com.example.proiect.Services.CategoryServices.InsertCategoryService;
import com.example.proiect.Services.ProductServices.InsertProductService;
import com.example.proiect.Services.ProductServices.ProductServices;
import com.example.proiect.Services.SubcategoryServices.InsertSubcategoryService;
import com.example.proiect.Services.SubcategoryServices.SubcategoryServices;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCartFragment extends Fragment implements CategoryServices, SubcategoryServices, ProductServices, OnItemClickListener<Product> {

    Button insertCategoriesBtn;
    Button insertSubcategoriesBtn;
    Button insertProductsBtn;

    public static List<Product> productsInCart = new ArrayList<>();
    public RecyclerView recyclerView;
    public ShoppingCartProductsAdapter shoppingCartProductsAdapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_shopping_cart, container, false);

        /*insertCategoriesBtn = (Button)view.findViewById(R.id.insertCategoriesBtn);
        insertCategoriesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertCategories();
            }
        });
        insertSubcategoriesBtn = (Button)view.findViewById(R.id.insertSubcategoriesBtn);
        insertSubcategoriesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertSubcategories();
            }
        });
        insertProductsBtn = (Button)view.findViewById(R.id.insertProductsBtn);
        insertProductsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertProducts();
            }
        });*/
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putAll(outState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recycler_cart);
        shoppingCartProductsAdapter = new ShoppingCartProductsAdapter(productsInCart, this, view);
        shoppingCartProductsAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(shoppingCartProductsAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    public void insertCategories() {
        Category electronics = new Category(
            "Electronics"
        );

        Category clothes = new Category(
                "Clothes"
        );

        Category homeAccessories = new Category(
                "Home Accessories"
        );

        Category cosmetics = new Category(
                "Cosmetics"
        );

        Category[] categories = {electronics, clothes, homeAccessories, cosmetics};

        new InsertCategoryService(this).execute(categories);
    }

    public void insertSubcategories() {
        Subcategory Laptops = new Subcategory(
            1,
                "Laptops"
        );

        Subcategory Phones = new Subcategory(
                1,
                "Phones"
        );
        Subcategory Tablets = new Subcategory(
                1,
                "Tablets"
        );
        Subcategory PlayStation = new Subcategory(
                1,
                "PlayStation"
        );
        Subcategory Tv = new Subcategory(
                1,
                "TV"
        );

        Subcategory Cameras = new Subcategory(
                1,
                "Cameras"
        );

        Subcategory Jackets = new Subcategory(
                2,
                "Jackets"
        );
        Subcategory Sweaters = new Subcategory(
                2,
                "Sweaters"
        );
        Subcategory Shirts = new Subcategory(
                2,
                "Shirts"
        );
        Subcategory Pants = new Subcategory(
                2,
                "Pants"
        );
        Subcategory Shoes = new Subcategory(
                2,
                "Shoes"
        );

        Subcategory Garden = new Subcategory(
                3,
                "Garden"
        );

        Subcategory Furniture = new Subcategory(
                3,
                "Furniture"
        );
        Subcategory Bathroom = new Subcategory(
                3,
                "Bathroom"
        );
        Subcategory Kitchen = new Subcategory(
                3,
                "Kitchen"
        );
        Subcategory Textiles = new Subcategory(
                3,
                "Textiles"
        );

        Subcategory Face = new Subcategory(
                4,
                "Face"
        );
        Subcategory Body = new Subcategory(
                4,
                "Body"
        );
        Subcategory Hair = new Subcategory(
                4,
                "Hair"
        );
        Subcategory Oral = new Subcategory(
                4,
                "Oral hygiene"
        );

        Subcategory[] subcategories = {Laptops, Phones, Tablets, PlayStation, Tv, Cameras, Jackets, Sweaters, Shirts, Pants, Shoes, Garden, Furniture, Bathroom, Kitchen, Textiles, Face, Body, Hair, Oral};

        new InsertSubcategoryService(this).execute(subcategories);
    }

    public void insertProducts() {
        // implement like the others
        Product macbook = new Product(
                "Laptop Apple MacBook Air 13-inch",
                "Laptop Apple MacBook Air 13-inch, True Tone, procesor Apple M1 , 8 nuclee CPU si 7 nuclee GPU, 8GB, 256GB, Space Grey, INT KB",
                5000,
                20,
                1
        );

        Product asus = new Product(
                "Laptop Gaming ASUS ROG Strix SCAR 17",
                "Laptop Gaming ASUS ROG Strix SCAR 17 cu procesor AMD Ryzen™ 9 5900HX pana la 4.60 GHz, 17.3\", Full HD, 300Hz, 64GB, 1TB SSD, NVIDIA® GeForce RTX™ 3080 16GB, Free DOS, Black",
                15000,
                10,
                1
        );

        Product lenovo = new Product(
                "Laptop Lenovo IdeaPad 3",
                "Laptop Lenovo IdeaPad 3 15IML05 cu procesor Intel® Celeron® 5205U, 15.6\" HD, 4GB, 128GB SSD, Intel® UHD Graphics, Windows 10 Home S, Platinum Grey",
                1700,
                35,
                1
        );

        Product iphone = new Product(
                "Apple iPhone 12 Pro",
                "Telefon mobil Apple iPhone 12 Pro, 128GB, 5G, Graphite",
                5050,
                13,
                2
        );
        Product samsung = new Product(
                "Samsung Galaxy S20 FE",
                "Telefon mobil Samsung Galaxy S20 FE, Dual SIM, 128GB, 6GB RAM, 5G, Cloud Navy\n",
                2070,
                11,
                2
        );
        Product huawei = new Product(
                "Huawei Mate 40 Pro",
                "Telefon mobil Huawei Mate 40 Pro, Dual SIM, 256GB, 8GB RAM, 5G, Mystic Silver",
                4650,
                16,
                2
        );

        // continue the same way

        Product[] products = {macbook, asus, lenovo, iphone, samsung, huawei};

        new InsertProductService(this).execute(products);

    }

    @Override
    public void categoryOperationResult(String result) {
        Toast.makeText(getContext(),result, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void productOperationResult(String result) {
        Toast.makeText(getContext(),result, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void subcategoryOperationResult(String result) {
        Toast.makeText(getContext(), result, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void getSubcategoriesByParentId(List<Subcategory> subcategoryList) {
    }

    @Override
    public void getProductsBySubcategoryId(List<Product> productList) {
    }

    @Override
    public void onItemClick(Product item) {
        productsInCart.remove(item);
        shoppingCartProductsAdapter.notifyDataSetChanged();
    }
}
