package com.example.proiect.Screens;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.proiect.Activities.Home;
import com.example.proiect.Activities.MainActivity;
import com.example.proiect.Entities.User;
import com.example.proiect.R;
import com.example.proiect.Services.UserServices.InsertUserService;
import com.example.proiect.Services.UserServices.UserServices;

public class Register extends AppCompatActivity implements UserServices {

    EditText inputFirstName;
    EditText inputLastName;
    EditText inputEmail;
    EditText inputPassword;
    EditText inputConfirmPass;

    Button buttonRegister;

    public String getInputFirstName() {
        return inputFirstName.getText().toString();
    }

    public String getInputLastName() {
        return inputLastName.getText().toString();
    }

    public String getInputEmail() {
        return inputEmail.getText().toString();
    }

    public String getInputPassword() {
        return inputPassword.getText().toString();
    }

    public String getInputConfirmPass() {
        return inputConfirmPass.getText().toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_screen);

        inputFirstName = (EditText)findViewById(R.id.firstName);
        inputLastName = (EditText)findViewById(R.id.lastName);
        inputEmail = (EditText)findViewById(R.id.email);
        inputPassword = (EditText)findViewById(R.id.password);
        inputConfirmPass = (EditText)findViewById(R.id.confirmPass);

        buttonRegister = (Button)findViewById(R.id.registerButton);

        buttonRegister.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                insertUser();
            }
        });
    }

    public Boolean ValidateConfirmPassword() {
        if(!getInputConfirmPass().equals(getInputPassword())) {
            inputConfirmPass.setError("Confirm password and password must be the same");
            return false;
        }
        return true;
    }

    private void insertUser() {

        if(ValidateConfirmPassword()) {
            User user = new User(
                getInputFirstName(),
                getInputLastName(),
                getInputEmail(),
                getInputPassword()
            );

            new InsertUserService(this).execute(user);
        }
    }

    @Override
    public void userOperationResult(String result) {
        Toast.makeText(this, result, Toast.LENGTH_LONG).show();
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivityForResult(myIntent, 0);
    }
}
