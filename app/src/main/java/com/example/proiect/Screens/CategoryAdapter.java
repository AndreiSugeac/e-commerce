package com.example.proiect.Screens;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.proiect.Entities.Category;
import com.example.proiect.R;

import org.w3c.dom.Text;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {

    private List<Category> dataSet;
    public static OnItemClickListener itemClickListener;
    // the adapter takes the list: creates a viewholder, assigns it an element and bind...

    public CategoryAdapter(List<Category> data, OnItemClickListener listener){
        dataSet = data;
        itemClickListener = listener;
    }
    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        holder.Bind(dataSet.get(position));
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }


    public static class CategoryViewHolder extends RecyclerView.ViewHolder{

        private final TextView textView;
        private final ImageView imageView;
        private final ConstraintLayout categoryContainer;

        public CategoryViewHolder(View view){
            super(view);
            textView = view.findViewById(R.id.title);
            imageView = view.findViewById(R.id.image);
            categoryContainer= view.findViewById(R.id.category_container);
        }

        public void Bind(Category item){
            textView.setText(item.categoryName);
            switch (item.categoryId){
                case 1:
                    imageView.setImageResource(R.drawable.electronics);
                    break;
                case 2:
                    imageView.setImageResource(R.drawable.clothes_icon);
                    break;
                case 3:
                    imageView.setImageResource(R.drawable.home_garden);
                    break;
                case 4:
                    imageView.setImageResource(R.drawable.cosmetics);
                    break;
            }
          //  imageView.setImageDrawable(Drawable.createFromPath("@drawable/clothes"));
          //  imageView.setImageDrawable(ContextCompat.getDrawable(imageView.getContext(), ));
            categoryContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // announce fragment that we've clicked on an element
                    //triggers onItemClick in fragment
                    itemClickListener.onItemClick(item);
                }
            });
        }
    }
}
