package com.example.proiect.Screens;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.proiect.Entities.Product;
import com.example.proiect.Entities.Subcategory;
import com.example.proiect.R;

import java.util.List;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductViewHolder>{
    private List<Product> dataSet;
     public static OnItemClickListener itemClickListener;
    // the adapter takes the list: creates a viewholder, assigns it an element and bind...

    public ProductsAdapter(List<Product> data, OnItemClickListener listener){ // + , OnItemClickListener listener
        dataSet = data;
        itemClickListener = listener;
    }
    @NonNull
    @Override
    public ProductsAdapter.ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);
        return new ProductViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull ProductsAdapter.ProductViewHolder holder, int position) {

        holder.Bind(dataSet.get(position));
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class ProductViewHolder extends RecyclerView.ViewHolder{

        private final TextView textView;
        private final ImageView imageView;
        private final ConstraintLayout productContainer;


        public ProductViewHolder(View view){
            super(view);

            textView = view.findViewById(R.id.title);
            imageView = view.findViewById(R.id.image);
            productContainer= view.findViewById(R.id.product_container);
        }
        public void Bind(Product item){

            textView.setText(item.productName);
            switch (item.productId){
                case 1:
                    imageView.setImageResource(R.drawable.macbook);
                    break;
                case 2:
                    imageView.setImageResource(R.drawable.asus);
                    break;
                case 3:
                    imageView.setImageResource(R.drawable.lenovo);
                    break;
                case 4:
                    imageView.setImageResource(R.drawable.iphone);
                    break;
                case 5:
                    imageView.setImageResource(R.drawable.samsung);
                    break;
                case 6:
                    imageView.setImageResource(R.drawable.huawei);
                    break;
            }
             productContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                 public void onClick(View v) {
            // announce fragment that we've clicked on an element
            //triggers onItemClick in fragment
                    itemClickListener.onItemClick(item);
                 }
             });
        }
    }
}
